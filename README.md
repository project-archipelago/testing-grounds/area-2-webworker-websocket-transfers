# Area 2 - Webworker Websocket Transfers

The question was if you could transfer websockets to webworkers.

As of Nov 10 2020, No.

See [THIS ISSUE](https://github.com/denoland/deno/issues/8341). When you can transfer resources this will need to be checked again.