import {
    Application
} from "https://deno.land/x/oak@v6.3.1/mod.ts";
import { WebSocketMiddleware, WebSocket, handler } from "https://raw.githubusercontent.com/jcc10/oak_websoket_middleware/v0.1.0/mod.ts";

const rooms = {
    "a": new Worker(new URL("worker.ts", import.meta.url).href, { type: "module" }),
    "b": new Worker(new URL("worker.ts", import.meta.url).href, { type: "module" }),
}

async function socket_handler(socket: WebSocket, url:URL): Promise<void> {
    if(url.pathname === "/a"){
        // HI, The answer is no.
        rooms.a.postMessage("newUser", [socket]){

        }
    }
}

const app = new Application();
const ws_server = new WebSocketMiddleware(socket_handler);
app.use(ws_server.middleware());

app.use(async (ctx: any, next: any) => {
    ctx.response.body = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <title>Chat using Deno</title>
</head>
<body>
    <input id="test"><button id="send">send</button>
    <div id="messages"></div>
    <script>
        const test = document.getElementById("test")
        const send = document.getElementById("send");
        const messages = document.getElementById("messages");
        const socket = new WebSocket(\`ws://\${window.location.host}/ws\`);
    socket.onmessage = (event) => {
        messages.innerHTML = event.data + "<br />" + messages.innerHTML;
    }
    send.onclick = () => {
        socket.send(test.value);
    }
    </script>
        </body>
        </html>`
});
console.log("Server running on localhost:3000");
await app.listen({ port: 3000 });